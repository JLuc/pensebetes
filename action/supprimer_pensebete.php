<?php
/**
 * Gestion de l'action supprimer_pensebete
 *
 * @plugin Pense-bêtes
 * @copyright  2019-2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\Pensebetes\Actions
 */
 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour supprimer un Pense-bête
 *
 * En réalité, c'est la déclaration de l'auteur comme receveur du pense-bête
 * qui est supprimée, ce qui fait qu'il ne s'affiche plus (pour lui).
 * Si l'auteur était le dernier receveur, alors le 
 * pense-bête effectivement est supprimé.
 *
 * @param  int    $id_pensebete    Identifiant de l'objet
 * @return void
**/
 
function action_supprimer_pensebete_dist($id_pensebete=null){

	if (is_null($id_pensebete)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_pensebete = $securiser_action();
	}

	if (!autoriser('pensebete_supprimer', 'pensebete', $id_pensebete)) {
		include_spip('inc/minipres');
		minipres(_T('erreur'),_T('pensebete:erreur_suppression'));
		exit;
	}	

	$id_auteur = $GLOBALS['auteur_session']['id_auteur'];
	// Le demandeur de la suppression est-il le donneur du pense-bête ?
	if (
		$id_donneur = sql_getfetsel('id_donneur', 'spip_pensebetes', 'id_pensebete=' . intval($id_pensebete))
		and
		$id_donneur == $id_auteur
	) {
		$sup_receveur = sql_delete('spip_pensebetes_receveurs', array('id_pensebete=' . intval($id_pensebete)));
		$sup_pensebete = sql_delete('spip_pensebetes', array('id_pensebete=' . intval($id_pensebete)));
	} else {
		// le receveur ne veut plus être destinataire du pense-bête
		$sup_receveur = sql_delete('spip_pensebetes_receveurs',array('id_pensebete=' . intval($id_pensebete), 'id_receveur=' . $id_auteur));
		// si le receveur était le dernier destinataire du pense-bête, on supprime le pense-bête
		$encore = sql_countsel('spip_pensebetes_receveurs', 'id_pensebete=' . intval($id_pensebete));
		if (!$encore) {
			$sup_pensebete = sql_delete('spip_pensebetes',array('id_pensebete=' . intval($id_pensebete)));
		}
	}
	
	if (!$sup_receveur) {
		spip_log(_T('pensebete:log_action_supprimer_receveur_imp', array( 'id' => intval($id_pensebete), 'aut' => $id_auteur)),'pensebetes.' . _LOG_ERREUR);
	} else {
		spip_log(_T('pensebete:log_action_supprimer_receveur', array('id' => intval($id_pensebete),'aut' => $id_auteur, 'nb' => $sup_receveur)),'pensebetes.' . _LOG_INFO_IMPORTANTE);
	}

	if (!$sup_pensebete) {
		spip_log(_T('pensebete:log_action_supprimer_pensebete_imp', array('id' => intval($id_pensebete), 'aut' => $id_auteur)),'pensebetes.' . _LOG_ERREUR);
	} else {
		spip_log(_T('pensebete:log_action_supprimer_pensebete', array('id' => intval($id_pensebete), 'aut' => $id_auteur)),'pensebetes.' . _LOG_INFO_IMPORTANTE);
		// supprimer les liaisons
		$sup_pensebete = sql_delete('spip_pensebetes_liens', array('id_pensebete=' . intval($id_pensebete)));
		}
	// si l'on est en train de visualiser le contenu du pense-bête
	// sa suppression ne permet pas la redirection prévue :
	if (_request('exec') == 'pensebete' and test_espace_prive()) {
		// Nous sommes dans l'espace privé et l'on regarde le contenu d'un pensebete
		// On le supprimer, il faut revenir vers la liste des pensesbetes
		include_spip('inc/headers');
		$redirect = generer_url_ecrire('pensebetes');
		redirige_par_entete($redirect);
	}
	// Invalider les caches
	// pour que la page soit recalculée et que le pense-bête disparaisse.
	include_spip('inc/invalideur');
	suivre_invalideur("id='pensebete/$id_pensebete'");
}

?>