<?php

/**
 * Gestion de l'action `editer_pensebete` et des fonctions d'insertion
 * et modification des pense-bêtes
 *
 * @plugin Pense-bêtes
 * @license GPL (c) 2019 - 2021
 * @author Vincent CALLIES
 *
 * @package SPIP\Pensebetes\Actions
**/

// Sécurité
if (! defined('_ECRIRE_INC_VERSION')) return;

/**
 * Action de création / modification d'un pense-bête
 *
 * @param null|int $arg
 *     Identifiant du pense-bête.
 *     En absence utilise l'argument de l'action sécurisée.
 * @return array
 *     Liste (identifiant du pense-bête, Texte d'erreur éventuel)
 */
function action_editer_pensebete_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// si id_pensebete n'est pas un nombre, c'est une creation
	if (!$id_pensebete = intval($arg)) {
		$id_pensebete = pensebete_inserer();
	}

	// Enregistre l'envoi dans la BD
	if ($id_pensebete > 0) {
		$err = pensebete_modifier($id_pensebete);
	}

	return array($id_pensebete, $err);
}

/**
 * Crée un nouveau pense-bête et retourne son ID
 *
 * @pipeline_appel pre_insertion
 * @pipeline_appel post_insertion
 *
 * @param array $champs
 *     Un tableau avec les champs par défaut lors de l'insertion
 * @return int
 *     Identifiant du pense-bête créé
 */
function pensebete_inserer($id_parent = null, $champs = array()) {

	// Envoyer aux plugins avant insertion
	$champs = pipeline(
		'pre_insertion',
		array(
			'args' => array(
				'table' => 'spip_pensebetes',
			),
			'data' => $champs
		)
	);

	// Insérer l'objet
	$id_pensebete = sql_insertq('spip_pensebetes', $champs);

	// Envoyer aux plugins après insertion
	pipeline(
		'post_insertion',
		array(
			'args' => array(
				'table' => 'spip_pensebetes',
				'id_objet' => $id_pensebete
			),
			'data' => $champs
		)
	);

	return $id_pensebete;
}


/**
 * Modifie les données d'un pense-bête
 *
 * Récupère les valeurs qui ont été postées d'un formulaire d'édition
 * automatiquement.
 *
 * @param int $id_pensebete
 *     Identifiant du pense-bête
 * @param null|array $set
 *     Couples de valeurs à affecter d'office
 * @return string
 *     Vide en cas de succès, texte d'erreur sinon.
 */
function pensebete_modifier($id_pensebete, $set = null) {

	include_spip('inc/modifier');
	include_spip('inc/filtres');
	include_spip('action/editer_objet');

	$c = collecter_requests(
		// white list
		objet_info('pensebete', 'champs_editables'),
		// black list
		array(),
		// donnees eventuellement fournies
		$set
	);

	if ($err = objet_modifier_champs(
		'pensebete',
		$id_pensebete,
		array(
			'data' => $set,
		),
		$c
	)) {
		return $err;
	}

	// peupler la table auxiliaires
	$auteurs = array();

	// on récupère les auteurs des statuts
	if ($listes_receveurs = _request('listes_receveurs')) {
		$tableau = sql_allfetsel('id_auteur', 'spip_auteurs',  sql_in('statut', $listes_receveurs));
		// transformation du format 0/id_auteur/1
		foreach ($tableau as $auteur) {
			$auteurs[] = $auteur['id_auteur'];
		}
	}

	// on récupère le tableau de données des auteurs et le fusionne avec celui des statuts si nécessaire
	if ($auteurs_receveurs = _request('auteurs_receveurs')) {
		if ($auteurs) {
			$auteurs = array_merge($auteurs,$auteurs_receveurs);
			// on dédoublonne le tableau
			$auteurs = array_unique($auteurs);
		} else {
				$auteurs = $auteurs_receveurs;
		}
	}

	// on indique les auteurs auxquels le pense-bête est destiné dans la table auxiliaire.
	sql_delete('spip_pensebetes_receveurs', 'id_pensebete=' . intval($id_pensebete));
	if (!is_array($auteurs)) { // si config destinataire = non multiple
		$auteurs = array($auteurs);
	}
	foreach ($auteurs as $key => $id_auteur) {
		sql_insertq ('spip_pensebetes_receveurs',
			array(
				'id_receveur' => $id_auteur,
				'id_pensebete' => $id_pensebete
			)
		);
	}

	$c = collecter_requests(array('statut'), array(), $set);

	return objet_instituer('pensebete', $id_pensebete, $c);
}
