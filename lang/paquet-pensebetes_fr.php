<?php
/**
 * Variables de langue du Plugin Pensebetes
 *
 * @plugin Pensebetes
 * @copyright  2019-2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\Pensebetes\Lang
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'pensebetes_nom' => 'Pense-bêtes (test dev)',
	'pensebetes_slogan' => 'Un plugin qui accroche !',
	'pensebetes_description' => 'Un Plugin destinée à se rappeler ce qu’on a l’intention de faire ou à rappeler à quelqu’un ce qu’il doit faire : Pense-bête mural pour la partie privée de SPIP.',

);

?>