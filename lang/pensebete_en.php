<?php
/**
 * Définit les variables de langue du plugin Pensebetes (anglais / english)
 *
 * @plugin     Pensebetes
 * @copyright  2019-2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pensebetes\Lang
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'log_action_supprimer_pensebete_imp' => 'action_supprimer_pensebete_dist : the deletion by author n°@aut@ of reminder n°@id@ from the "spip_pensebetes" table proved impossible.',
	'cfg_boites' => 'Information boxes',
	'cfg_boites_explication' => 'Activity boxes for sent or received reminders displayed laterally on:',
	'cfg_boites_explication'=>'Activity box on the side of :',
	'cfg_corps' => 'Number of characters for text',
	'cfg_espace_public' => 'Style in the public part',
	'cfg_espace_public' => 'The sticky note can be visualized, by incorporating a MODELE.',
	'cfg_explication_espace_privee' => 'The sticky note is a means of communication between the authors, invisible from the public. Indicate below the objects on which your authors can post sticky notes.',
	'cfg_explication_espace_public' => 'Installing the plugin’s private style sheet in the public part allows you to benefit from the appearance of the sticky note as you see it in the private part (This is only a configuration option as your theme for the public space could provide a different appearance).',
	'cfg_height' => 'Sticky note height',
	'cfg_la_class' => 'Define the height and width of the sticky note',
	'cfg_la_class_explication' => 'Size matters, they say. We therefore let you define the height and width of the Sticky Note (the default values during installation are 12em and 12em)',
	'cfg_les_caracteres' => 'Define the number of characters for the sticky note',
	'cfg_les_caracteres_explication' => 'Others say it’s the character that counts. We therefore let you define the number of characters in the Sticky Notes (the default values are 17 for the title and 110 for the body).',
	'cfg_lieux'=>'Sticky notes in (places):',
	'cfg_lieux_explication' => "Display author's notes on the pages:",
	'cfg_listes' => 'List of authors',
	'cfg_listes_explication' => 'Allow to post a reminder on the walls of a selection of authors',
	'cfg_multiple' => 'Multiple selection of authors',
	'cfg_multiple_explication' => 'Allow you to put the same reminder on the walls of several authors',
	'cfg_objets'=>'Sticky notes on (objects):',
	'cfg_public' => 'Public space',
	'cfg_statuts' => 'Status of authors who can exchange sticky notes',
	'cfg_statuts_explication' => 'Be careful if you validate visitors (6forum), as this requires handling of sticky notes in the public space',
	'cfg_taille' => 'Size matters ...',
	'cfg_titre' => 'Number of characters for the title',
	'cfg_width' => 'Sticky note width',
	'erreur_association'=>'The sticky note is created (n° @id_pensebete@) but the association with the editorial object could not be made.',
	'erreur_suppression'=> 'You do not have permission to delete this sticky note',
	'explication_texte'=> 'Your text must have gone straight to the point.',
	'explication_titre'=> 'Your title must be brief.',
	'icone_creer_pensebete' => 'Create a sticky note',
	'icone_modifier_pensebete' => 'Edit this sticky note',
	'info_1_pensebete' => 'A sticky note',
	'info_1_pensebete_donne' => 'One sticky note given',
	'info_1_pensebete_recu' => 'One sticky note received',
	'info_aucun_pensebete' => 'No sticky note',
	'info_aucun_pensebete_donne'=>'No sticky note given',
	'info_aucun_pensebete_recu'=>'No sticky note received',
	'info_lassociation'=>'The association',
	'info_le_pensebete' => 'Your sticky note',
	'info_nb_pensebetes' => '@nb@ sticky notes',
	'info_nb_pensebetes_donnes' => '@nb@ sticky notes given',
	'info_nb_pensebetes_recus' => '@nb@ sticky notes received',
	'label_a' => 'to',
	'label_de' => 'From',
	'label_donneur' => 'From',
	'label_infos' => 'Information',
	'label_listes' => 'Lists',
	'label_receveur' => 'To',
	'label_titre' => 'Title',
	'lien_ajouter_pensebete' => 'Add this sticky note',
	'lien_retirer_pensebete' => 'Remove this sticky note',
	'lien_retirer_pensebetes' => 'Remove all sticky notes',
	'log_action_supprimer_pensebete' => 'action_supprimer_pensebete_dist : deletion by author n°@aut@ of sticky note n° @id@ from the "spip_pensebetes" table.',
	'log_action_supprimer_receveur' => 'action_supprimer_pensebete_dist : deletion by author n°@aut@ of @nb@ entries) in the "spip_pensebetes_receveurs" table for reminder n°@id@.',
	'log_action_supprimer_receveur_imp' => 'action_supprimer_pensebete_dist : the deletion by the author n°@aut@ of registration of the "spip_pensebetes_receveurs" table for the reminder n°@id@ turned out to be impossible',
	'saisies_obligatoire_receveur' => 'The reminder must have a recipient.',
	'texte_ajouter_pensebete' => 'Add a sticky note',
	'texte_association'=> 'Association',
	'texte_associer_pensebete'=> 'Do you want to associate this sticky note ti this object ?',
	'texte_avertissement_retrait' => 'Are you sure you want to remove this sticky note?',
	'texte_changer_statut' => 'This sticky note is :',
	'texte_creer_associer_pensebete'=> 'Create and associate a sticky note',
	'texte_nouveau_pensebete'=> 'New sticky note',
	'titre_activite_mur' => 'Wall activity',
	'titre_langue_pensebete' => 'Language of this sticky note',
	'titre_logo_pensebete' => 'Sticky note logo',
	'titre_mur_de' => 'on @auteur@’s wall',
	'titre_mur_mien' => 'on my wall',
	'titre_murs' => 'Sticky notes...',
	'titre_murs_autres' => 'on the neighbors’ walls',
	'titre_pensebete' => 'Sticky note',
	'titre_pensebetes' => 'Sticky notes',
	'titre_pensebetes_rubrique' => 'Sticky notes of the rubric',
	'titre_sur_mur' => 'on my wall from @auteur@',

);

?>
