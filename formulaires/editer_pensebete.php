<?php
/**
 * Gestion du formulaire de d'édition de pensebete
 *
 * @plugin Pensebetes
 * @copyright  2020-2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\Pensebetes\Formulaires
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement des valeurs de SAISIES
 *
 * Aide à la création du formulaire avec le plugin SAISIES
 *
 * Les valeurs sont calculées en fonction de la configuration
 * mais aussi d'éventuelles autorisations qui permettent de gérer plus finement
 * la configuration.
 *
 * @var string $icone_objet
 *     pour afficher si nécessaire l'icône de l'objet auquel le pense-bête sera lié
 *
 */
function formulaires_editer_pensebete_saisies_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	include_spip('inc/config'); // pour la fonction lire_config()
	$maxlength_titre = lire_config("pensebetes/titre", 17);
	$maxlength_corps = lire_config("pensebetes/corps", 110);
	$id_pensebete = intval($id_pensebete);

	# A-t-on demandé à ce que le pense-bête soit associé à un objet éditorial ?
	if ($associer_objet) {
		list($objet, ) = explode('|', $associer_objet);
		$icone_objet = chemin_image($objet);
		$c_associe = 'oui';
	} else {
		$c_associe = 'non';
		$icone_objet= '';
	}

	# entre quels auteurs peuvent s'échanger des Penses-bêtes ?
	$mes_statuts = lire_config("pensebetes/mes_statuts");
	if (!$mes_statuts) {
		$mes_statuts = array('0minirezo', '1comite');
	}
	$total = '';

	include_spip('inc/autoriser');
	# choisir les auteurs par des listes par statuts
	if (
		(!$id_pensebete and autoriser('creer', 'pensebete', '', '', array('universel' => 'oui')))
			or
		($id_pensebete and autoriser('modifier', 'pensebete', $id_pensebete, '', array('universel' => 'oui')))
	) {
		$liste_possible = objet_info('spip_auteurs','statut_titres');
		$listes = array();
		unset($liste_possible[0]); // titre_image_visiteur
		foreach ($liste_possible as $cle => $idiome){
			if (in_array($cle, $mes_statuts)){
				$listes[$cle] =  _T($idiome);
			}
		}
		// prévoir l'affichage conditionnel de auteurs_receveurs
		// @listes_receveurs@ == "0minirezo" && @listes_receveurs@ == "1comite"
		// doc = https://contrib.spip.net/Affichage-conditionnel-de-saisie-syntaxe-des-tests
		$condition_debut = '@listes_receveurs@ != "';
		$condition_fin = '" || ';
		foreach ($mes_statuts as $cle){
			$total .=  $condition_debut . $cle . $condition_fin;
		}
		$total = rtrim($total, ' |');
		$listes_receveurs = array( 
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'listes_receveurs',
				'label' => _T('pensebete:label_listes'),
				'obligatoire' => 'non',
				'multiple' => 'oui',
				'data' => $listes
				)
			);
	} else {
		$listes_receveurs = array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'listes_receveurs',
				'defaut' => ''
			)
		);
	}

	# choisir les auteurs par une selection multiple 
	if (
		(!$id_pensebete and autoriser('creer', 'pensebete', '', '', array('multiple' => 'oui')))
			or
		($id_pensebete and autoriser('modifier', 'pensebete', $id_pensebete, '', array('multiple' => 'oui')))
	) {
		$multiple = 'oui';
	} else {
		$multiple = '';
	}
	$auteurs_receveurs = array(
		'saisie' => 'auteurs',
		'options' => array(
			'nom' => 'auteurs_receveurs',
			'label' => _T('pensebete:label_receveur'),
			'multiple' => $multiple,
			'cacher_option_intro' => 'on',
			'statut' => $mes_statuts,
			'afficher_si' => $total
		)
	);

	# définir les saisies dans un ou deux fieldsets, selon l'association ou non d'un objet
	$saisies = array(
		array( // le fieldset 
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'le_pensebete',
				'label' => _T('pensebete:info_le_pensebete'),
				'icone' => 'pensebete-24'
			),
			'saisies' => array( // les champs dans le fieldset 
				array( // champ retour nécessaire notamment à la création pour aller directement au contenu
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'retour'
					)
				),
				array( // champ id_pensebete (numéro unique du pense-bête)
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'id_pensebete',
						'defaut' => $id_pensebete
					)
				),
				array( // champ id_donneur (numéro identification auteur du pense-bête)
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'id_donneur',
						'defaut' => 0
					)
				),
				array( // champ date (date de création du pense-bête)
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'date',
					)
				),
				array( // champ associer_objet (A-t-on demandé que ce pense-bête soit associé à un objet )
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'associer_objet',
						'defaut' => $associer_objet
					)
				), $auteurs_receveurs,$listes_receveurs,
				array( // champ titre : ligne de texte
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titre',
						'label' => _T('pensebete:label_titre'),
						'maxlength' => $maxlength_titre, // limiter la taille du titre
						'obligatoire' => 'oui',
					),
					'verifier' => array(
						'type' => 'taille',
						'options' => array ('min' => 0, 'max' => $maxlength_titre),
					)
				),
				array( // champ texte : un bloc de texte
					'saisie' => 'textarea',
					'options' => array(
						'nom' => 'texte',
						'label' => _T('message'),
						'rows' => 3,
						'cols' => 60,
						'longueur_max' => $maxlength_corps, // limiter la taille du texte
					),
					'verifier' => array(
						'type' => 'taille',
						'options' => array ('min' => 0, 'max' => $maxlength_corps)
					),
				 ),
			),
		)
	);
	if ($associer_objet) {
		$saisies[] = array( // le fieldset
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'lassociation',
				'label' => _T ('pensebete:info_lassociation'),
				'icone' => $icone_objet,
			),
			'saisies' => array( // les champs dans le second fieldset
				array( // hors fieldset : association
					'saisie' => 'oui_non',
					'options' => array(
						'nom' => 'c_associe',
						'label' => _T ('pensebete:texte_associer_pensebete'),
						'valeur_oui' => 'oui',
						'valeur_non' => 'non',
						'defaut' => $c_associe
					)
				),
			)
		);
	}

	return $saisies;
}


/**
 * Chargement du formulaire d'édition de pensebete
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 * @uses formulaires_editer_objet_charger()
 * @uses mes_saisies_pensebete()
 *
 */
function formulaires_editer_pensebete_charger_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('pensebete',$id_pensebete,$id_parent,$lier_trad,$retour,$config_fonc,$row,$hidden);
	# si c'est une création, présenter au mieux
	if (!intval($id_pensebete)){
		# donner la date d'aujourd'hui
		$valeurs['date'] = date('Y-m-d H:i:s');
		# s'il n'y a pas d'id_donneur, donner l'id de l'auteur
		if (!_request('id_donneur')) {
			$valeurs['id_donneur'] = $GLOBALS['visiteur_session']['id_auteur'] ?? 0;
		}
		# Si titre dans l'url : fixer le titre (à 17 caractères)
		if (strlen($titre = _request('titre'))) {
			$valeurs['titre'] = substr ($titre, 0, 16);
		}
	} else {
	# si c'est une modification, présenter les auteurs destinataires
		$tableau = sql_allfetsel('id_receveur', 'spip_pensebetes_receveurs',  'id_pensebete=' . intval($id_pensebete));
		$auteurs = array();
		foreach ($tableau as $ligne) {
			$auteurs[] = $ligne['id_receveur'];
		}
		$valeurs['auteurs_receveurs'] = $auteurs;
	}
	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 */
function formulaires_editer_pensebete_identifier_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_pensebete), $associer_objet));
}

/**
 * Vérification du formulaire d'édition de pensebete
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 *
 */
function formulaires_editer_pensebete_verifier_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();//formulaires_editer_objet_verifier('pensebete',$id_pensebete);
	// Il faut au moins un pour ou un liste
	if (!_request('auteurs_receveurs') and !_request('listes_receveurs')) {
		$erreurs['message_erreur'] = _T('pensebete:saisies_obligatoire_receveur');
	}
	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de pensebete
 *
 * Le traitement effectue une mise à zéro de l'id_auteur pour éviter des associations considérées comme inutiles.
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 * @uses objet_inserer()
 * @uses objet_associer()
 *
 */
function formulaires_editer_pensebete_traiter_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$retours = array();
	// éviter que l'auteur soit associé au pense-bête.
	set_request('id_auteur','');

	// rompre l'association si nécessaire		
	if (_request('c_associe') == 'non'){
		set_request('associer_objet','');
	} else { // on garde la donnée de redirection s'il y a une association
		$redirect = _request('redirect');
	}

	// on traite les données
 	$retours = formulaires_editer_objet_traiter('pensebete', $id_pensebete, $id_parent, $lier_trad, $retour, $config_fonc, $row, $hidden);
	
	// si le pense-bête a été créé ou modifié avec succès, on se préoccupe d’associer le pensebete à un objet
	if (_request('associer_objet') and ($id_pensebete=$retours['id_pensebete'])) {
		if (intval(_request('associer_objet'))) {
			// compat avec l'appel de la forme ajouter_id_article
			$objet = 'article';
			$id_objet = intval(_request('associer_objet'));
		} else {
			list($objet, $id_objet) = explode('|', _request('associer_objet'));
		}
		if ($objet and $id_objet and autoriser('modifier', $objet, $id_objet)) {
			include_spip('action/editer_liens');
			objet_associer( array('pensebete' => $id_pensebete), array($objet => $id_objet) );
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_pensebete, '&');
			}
			return $retours;
		} else {
			return array('message_erreur'=>_T('pensebete:erreur_association'));
		}
	} // si remord par rapport à l'association à l'objet, rediriger vers le pense-bête
	elseif (_request('c_associe')=='non') {
		$retours['redirect'] = generer_url_ecrire ('pensebete', 'id_pensebete=' . $retours['id_pensebete']);
	}

	return $retours;	

}

