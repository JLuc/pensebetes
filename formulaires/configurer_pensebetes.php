<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_pensebetes_saisies_dist(){
	# $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration

	# limitation par statut des éditeurs pouvant échanger des penses-bêtes
	$statuts = objet_info('spip_auteurs','statut_titres');
	unset($statuts[0], $statuts['5poubelle']);
	foreach ($statuts as $cle => &$valeur){
		$idiome = _T($valeur,array(),array('force'=>''));
		if ($idiome){
			$valeur = $idiome;
		} else {
			$valeur = $cle;
		}
	}

	$saisies = array(
		array( // le fieldset sur les liaisons
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'liaisons_dangereuses',
				'label' => _T('espace_prive'),
			),
			'saisies' => array( // les champs dans le fieldset
				array(
					'saisie' => 'checkbox',
					'options' => array(
						'nom' => 'mes_statuts',
						'label' => _T('pensebete:cfg_statuts'),
						'explication' => _T('pensebete:cfg_statuts_explication'),
						'data' => $statuts,
						'obligatoire' => 'oui',
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'multiple',
						'label' => _T('pensebete:cfg_multiple'),
						'explication' => _T('pensebete:cfg_multiple_explication'),
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'listes',
						'label' => _T('pensebete:cfg_listes'),
						'explication' => _T('pensebete:cfg_listes_explication'),
					),
				),
				array(
					'saisie' => 'choisir_objets',
					'options' => array(
						'nom' => 'mes_objets',
						'label' => _T('pensebete:cfg_objets'),
						'explication' => _T('pensebete:cfg_explication_espace_privee'),
						'exclus' => array(
							'spip_pensebetes'
						),
					),
				),
				array(
					'saisie' => 'checkbox',
					'options' => array(
						'nom' => 'mes_lieux',
						'label' => _T('pensebete:cfg_lieux'),
						'explication' => _T('pensebete:cfg_lieux_explication'),
						'data' => array(
							'accueil' => _T('icone_accueil')
						),
					),
				),
				array(
					'saisie' => 'checkbox',
					'options' => array(
						'nom' => 'mes_boites',
						'label' => _T('pensebete:cfg_boites'),
						'explication' => _T('pensebete:cfg_boites_explication'),
						'data' => array(
							'accueil' => _T('icone_accueil'),
							'auteur' => _T('info_auteurs')
						),
					),
				),
			),
		),
		array( // le fieldset concernant la CSS dans l'espace public
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'cssoupas',
				'label' => _T('pensebete:cfg_public'),
			),
			'saisies' => array( // les champs dans le fieldset
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'espacepublic',
						'explication' => _T('pensebete:cfg_explication_espace_public'),
						'label_case' => _T('pensebete:cfg_espace_public'),
						'conteneur_class' => 'pleine_largeur'
					),
				),
			),
		),		
		array( // le fieldset sur la taille des pense-bête
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'la_taille_compte',
				'label' => _T('pensebete:cfg_taille'),
			),
			'saisies' => array( // les champs dans le fieldset
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'la_class',
						'titre' => _T('pensebete:cfg_la_class'),
						'texte' => _T('pensebete:cfg_la_class_explication'),
						'conteneur_class' => 'pleine_largeur'
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'height',
						'label' => _T('pensebete:cfg_height'),
					),
					'verifier' => array(
						'type' => 'regex',
						'options' => array('modele' => '@[0-9]+\.?[0-9]+(?:px|em|ex|%|in|cn|mm|pt|pc)@'),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'width',
						'label' => _T('pensebete:cfg_width'),
					),
					'verifier' => array(
						'type' => 'regex',
						'options' => array('modele' => '@[0-9]+\.?[0-9]+(?:px|em|ex|%|in|cn|mm|pt|pc)@'),
					),
				),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'les_caracteres',
						'titre' => _T('pensebete:cfg_les_caracteres'),
						'texte' => _T('pensebete:cfg_les_caracteres_explication'),
						'conteneur_class' => 'pleine_largeur'
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titre',
						'label' => _T('pensebete:cfg_titre'),
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array ('min' => 1, 'max' => 255),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'corps',
						'label' => _T('pensebete:cfg_corps'),
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array ('min' => 1, 'max' => 65535),
					),
				),
			),
		),
	);
	return $saisies;
}
