<?php
/**
 * Définit les autorisations du plugin Pensebetes
 *
 * @plugin     Pensebetes
 * @copyright  2019
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pensebetes\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function pensebetes_autoriser() {
}

/**
 * Autorisation d'associer un pensebete
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/

function autoriser_associerpensebetes_dist($faire, $type, $id, $qui, $opt) {
	return true;
}


/**
 * Fonctions relatives aux menus de l'interface privée
 *
 */

/**
 * Autorisation de voir un élément de menu (pensebetes)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebetes_menu_dist($faire, $type, $id, $qui, $opt) {
	 return true;
}


/**
 * Autorisation de voir le bouton d'accès rapide de création (pensebete)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebetecreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir le bouton d'outil collaboratif du mur
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_murs_menu_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array()));
}

/**
 * Fonctions relatives à l'Objet pensebete
 *
 */

/**
 * Autorisation de créer (pensebete)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_creer_dist($faire, $type, $id, $qui, $opt) {

	$donneur = TRUE;
	# limiter aux auteurs autorisés les listes et selections multiples dans la création de pense-bêtes
	if (isset($opt['universel']) and $opt['universel'] == 'oui') {
		/**
		 * pour permettre l'accès aux listes à tous, échangez :
		 * in_array($qui['statut'], array('0minirezo'))
		 * par in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array()))
		 */
		$donneur = (in_array($qui['statut'], array('0minirezo')) and (lire_config("pensebetes/listes") == 'on'));
	}
	# limiter aux auteurs autorisés la création d’un pense-bête sur les murs de plusieurs auteurs
	if (isset($opt['multiple']) and $opt['multiple'] == 'oui') {
		$donneur = (lire_config("pensebetes/multiple") == 'on');
	}

	#selon l'autorisation demandée, répondre
	return ($donneur AND in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array())));
}

/**
 * Autorisation de voir (pensebete)
 *
 * On peut voir les pensebetes dont on est l'auteur ou le destinataire.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_voir_dist($faire, $type, $id, $qui, $opt) {

	# il faut être un auteur pour avoir une autorisation, et celle-ci est systématiquement donnée à l'administrateur
	if (!$id_auteur = intval($qui['id_auteur'])) {
		return FALSE;
	} elseif ($qui['statut'] == '0minirezo') {
		return TRUE;
	}

	# demande d'une liste ou d'un pensebete ?
	$id_pensebete = intval($id);
	if (!$id_pensebete){
		return FALSE;
	}

	# entre quels auteurs peuvent s'échanger des Penses-bêtes ?
	$mes_statuts = lire_config("pensebetes/mes_statuts");
	if (!$mes_statuts) {
		$mes_statuts = array('0minirezo', '1comite');
	}
	if (!in_array($qui['statut'], $mes_statuts)){
		return FALSE;
	}

	# savoir qui le donneur du pense-bête
	if (!$id_donneur = intval($opt['id_donneur'])){
		$id_donneur = sql_getfetsel('id_donneur', 'spip_pensebetes', 'id_pensebete=' . $id_pensebete);
	}

	# on peut voir le pensebete que l'on a donné
	if ($qui['id_auteur'] == $id_donneur) {
		return true;
	}

	# savoir qui est le receveur du pense-bête
	$receveurs = array();
	if (!$id_receveur = intval($opt['id_receveur'])){
		if ($r = sql_select('id_receveur', 'spip_pensebetes_receveurs', 'id_pensebete='. $id_pensebete)) {
			while ($ligne = sql_fetch($r)) {
				$receveurs[] = $ligne['id_receveur'];
			}
		}
	}
	# on peut voir un pensebete dont on est le receveur
	if (in_array($qui['id_auteur'], $receveurs)) {
		return true;
	}

	return false;
}

/**
 * Autorisation de modifier (pensebete)
 *
 * On peut modifier que les pensebetes dont on est l'auteur.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_modifier_dist($faire, $type, $id, $qui, $opt) {
	# être un auteur autorisé demandant un pense-bête identifié ou être l'administrateur
	if (!$id_auteur = intval($qui['id_auteur']) or !(intval($id))) {
		return false;
	} elseif ($qui['statut'] == '0minirezo') {
		return true;
	}
	
	$id_donneur = sql_getfetsel('id_donneur', 'spip_pensebetes', 'id_pensebete=' . intval($id));
	if ($id_donneur == $qui['id_auteur']) {
		// on vérifie les options
		return (autoriser('creer','pensebetes','','',$opt));
	}

	return false;
}

/**
 * Autorisation de supprimer (pensebete)
 *
 * On peut supprimer tous les pensebetes que l'on peut voir.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser_pensebete_voir_dist($faire, $type, $id, $qui, $opt);
}

